package com.kafkaProducerConsumer.KafkaProducerConsumer.listners;

import com.kafkaProducerConsumer.KafkaProducerConsumer.commonUtills.CommonUtils;
import com.kafkaProducerConsumer.KafkaProducerConsumer.dto.Request;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ListnerTopicMyTopic {
    
    @KafkaListener(topics = "myTopic", groupId = "myGroupId")
    public void getMessageFromProducer(String request) {
        System.out.println("=-=-=-=-=-=-=-=-=-=" + request + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
        
        Request request1 = (CommonUtils.getGson().fromJson(request, Request.class));
        System.out.println("=-=-=-=-=-=-=-=-=-=" + request1 + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
    }
    
}
