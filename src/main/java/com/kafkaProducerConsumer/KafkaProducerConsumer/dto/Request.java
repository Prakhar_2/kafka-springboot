package com.kafkaProducerConsumer.KafkaProducerConsumer.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request {
    public String name;
    public String message;
    
}
