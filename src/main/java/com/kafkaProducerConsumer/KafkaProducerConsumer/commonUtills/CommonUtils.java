package com.kafkaProducerConsumer.KafkaProducerConsumer.commonUtills;

import com.google.gson.Gson;

public class CommonUtils {
    private static final Gson gson = new Gson();
    
    private CommonUtils() {
    }
    
    public static Gson getGson() {
        return gson;
    }
    
    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }
    
}