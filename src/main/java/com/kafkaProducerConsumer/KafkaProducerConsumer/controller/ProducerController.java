package com.kafkaProducerConsumer.KafkaProducerConsumer.controller;

import com.kafkaProducerConsumer.KafkaProducerConsumer.commonUtills.CommonUtils;
import com.kafkaProducerConsumer.KafkaProducerConsumer.dto.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {
    
    private final KafkaTemplate<Object, String> kafkaTemplate;
    
    
    @Autowired
    public ProducerController(KafkaTemplate<Object, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }
    
    @PostMapping(path = "api/kafkaProducerConsumer")
    public void post(@RequestBody Request request) {
        kafkaTemplate.send("myTopic", CommonUtils.getGson().toJson(request));
    }
}
